﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AsyncKeyword
{
    class Program
    {
        static void Main(string[] args)
        {
            var google = new Google();

            var content = google.GetContent(new Uri("http://www.google.com"));

            Console.Write(content);

            Console.Read();
        }
    }

    internal class Google
    {
        public string GetContent(Uri uri)
        {
            return "some html code";
        }
    }
}
